// essai git 1
function insere_nouvelle_question_ouverte(question,i){
    const formulaire = document.getElementById('quest');
    const para = document.createElement('p');
    const label = document.createElement('label');
    label.setAttribute('for', i);
    const enonce = document.createTextNode(question['enonce']);
    label.appendChild(enonce);
    para.appendChild(label);
    formulaire.appendChild(para);
    input = document.createElement('input');
    input.setAttribute('type', 'text');
    input.setAttribute('id', i);
    para.appendChild(input);
}

function insere_nouvelle_question_multiple(question,i){
	// on affiche l'énnoncé de la question
    const formulaire = document.getElementById('quest');
    const para = document.createElement('p');
    const label = document.createElement('label');
    label.setAttribute('for', i);
    const enonce = document.createTextNode(question['enonce']);
    label.appendChild(enonce);
    para.appendChild(label);
    formulaire.appendChild(para);
	for (let r=0;r<question['reponses'].length;r++){// pour chaque réponse possible on va créer une case a cocher
		input = document.createElement('input');
		input.setAttribute('type', 'checkbox');
		input.setAttribute('id', question['reponses'][r]['text']);
		//on donne a chaque case un nom et une valeur (réutilisés dans le score)
		input.setAttribute('name',question['reponses'][r]['text']);
		input.setAttribute('value',question['reponses'][r]['fraction']);
		//on crée le texte qui sera affiché à coté de la case
		let lab=document.createElement('label');
		let rep = document.createTextNode(question['reponses'][r]['text']);
		lab.appendChild(rep);
		para.appendChild(input);
		para.appendChild(lab);
	}	
}

function affiche(questions){
    for (let i=0;i<questions.length;i++){
        switch (questions[i]['type']) {
            case 'ouverte':
                insere_nouvelle_question_ouverte(questions[i],i+"");
                break;
            case 'multiple':
                insere_nouvelle_question_multiple(questions[i],i+"");
                break;
            default:
                console.log('Désolé, type ' + questions['type']  + 'inconnu !');
          }
    }
    const formulaire = document.getElementById('quest');
    const bouton = document.createElement('input');
    bouton.setAttribute('type', 'button');
    bouton.setAttribute('value', 'Envoyer réponses');
    bouton.onclick = function(){
        score(questions);
    }
    formulaire.appendChild(bouton);
}

function correction(questions, correct){
    for (let i=0;i<questions.length;i++){
		const affichage = document.getElementById('correc');
		const para = document.createElement('p');
		//const para2 = document.createElement('p');
		const enonce = document.createTextNode(questions[i]['enonce']);
		para.appendChild(enonce);
		let affiche=''; // variable contenant le résultat à afficher pour chaque question
		if (correct[i]==1){
			affiche='bonne réponse';
		
		}
		else
			affiche='mauvaise réponse';
		const evaluation = document.createTextNode(" "+affiche);
		para.appendChild(evaluation);
		affichage.appendChild(para);
		//affichage.appendChild(para2);
		
    }
    
}

function score(questions){
    let total = 0;
	let correct = new Array();// stock le résultat de chaque réponse
    for (let i=0;i<questions.length;i++){
        switch (questions[i]['type']) {
            case 'ouverte':
                const reponse_utilisateur = document.getElementById(i+"").value;
                if (questions[i]['reponses'][0]['text'] == reponse_utilisateur){
                    total += 1;
					// 1 = réponse bonne; par sécurité je met la valeur a 0 sinon
					correct[i]=1;
				}
				else
					correct[i]=0;		
			
                break;
            case 'multiple':
				let stotal=0;
				let erreur=0;
				for (let r=0;r<questions[i]['reponses'].length;r++){//passe en revu chaque réponse possible
					
					const reponse_utilisateur = document.getElementById(questions[i]['reponses'][r]['text']+"").value;//récupère la valeur de la réponse
					if (document.getElementById(questions[i]['reponses'][r]['text']+"").checked==true){
						if (reponse_utilisateur != 0)
							stotal += reponse_utilisateur;
						else
							erreur+=1;// une mauvaise case cochée = question ratée
					}
				}	
				
				//vérifie si toutes les bonnes réponses ont été cochées (on vérifie à moins de 100 pour les cas où le total des fractions seraient inférieur)
				if (stotal>90){
					
					if (erreur ==0){
						
						total +=1;
						correct[i]=1;
						
					}
					else
						correct[i]=0;
				}
				// todo
                break;
            default:
                console.log('Désolé, type ' + questions['type']  + 'inconnu !');
          }
    }
	correction(questions, correct);
    //alert("Votre score est de "+ total);
}

// Au chargement de la page, on charge les questions en JSON
// Ce n'est qu'au niveau du 2ème then que le contenu du fichier
// sera disponible 
// selon le navigateur utilisé, possible avec questions.json dans le répertoire courant
window.onload = function(){
    fetch("QCM-lili.json")
    .then(response => response.json())
    .then(json => affiche(json.questions)); 
}